<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-4.6.2-dist/css/bootstrap.min.css">
    <title>Tugas array</title>
</head>
<body>
    <!-- Soal
    kerjakan sesaui dengan gambar di bawah ini 
Tugas kedua sama dengan tugas array pada sumber code namun data di ubah 
data kids => susun dari pelatihan bootcamp umur 15-18 thn 
data adults => susun dari pelatihan bootcamp 18-22 thn 

 Susun data-data yang mengikuti bootcamp ke dalam bentuk Asosiatif Array didalam Array Multidimensi (seluruhnya)
    -->
    <!-- Just an image -->
<nav class="navbar navbar-success bg-light fixed">
  <a class="navbar-brand" href="#">
    <h1>Latihan Array</h1>
  </a>
</nav>
<!-- start from here -->
<div class="container shadow mt-4">
    <div class="row">
        <div class="col-4">
            <h3>Daftar Peserta bootcamp: </h3>
            <?php
            // start data
                $kids = array("Dika", "Alif", "Arkana", "Lucas", "Max", "Eleven");
                $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];

            // finish data
            // print data
                echo "Total anak-anak: " . count($kids);
                echo "<br>";
                echo "<ol>";
                echo "<li>$kids[0]</li>";
                echo "<li>$kids[1]</li>";
                echo "<li>$kids[2]</li>";
                echo "<li>$kids[3]</li>";
                echo "<li>$kids[4]</li>";
                echo "<li>$kids[5]</li>";
                echo "</ol>";
                echo "Total orang dewasa: " . count($adults);
                echo "<br>";
                echo "<ol>";
                echo "<li>$adults[0]</li>";
                echo "<li>$adults[1]</li>";
                echo "<li>$adults[2]</li>";
                echo "<li>$adults[3]</li>";
                echo "<li>$adults[4]</li>";
                echo "<ol/>";
            ?>
        </div>
        <div class="col-8">
            <h3>Array peserta bootcamp:</h3>
            <?php
             $data_peserta = array(
                ["name" => "Dika",
                 "umur" => 14,
                 "program" => "bootcamp",
                 "status" => "belum kawin"
                ],
                ["name" => "Nafar",
                 "umur" => 18,
                 "program" => "bootcamp",
                 "status" => "belum kawin"
                ],
                ["name" => "Ashfa",
                 "umur" => 18,
                 "program" => "bootcamp",
                 "status" => "belum kawin"
                ],
                ["name" => "Maulana",
                 "umur" => 19,
                 "program" => "bootcamp",
                 "status" => "belum kawin"
                ]
            );
            
            print_r($data_peserta);
            ?>
        </div>
    </div>
</div>

</body>
</html>