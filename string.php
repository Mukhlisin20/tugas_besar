<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-4.6.2-dist/css/bootstrap.min.css">
    <title>Tugas String</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <div class="card shadow">
                    <div class="card-header">
                        <h3 class="card-title">Tugas No 1 Menghitung jumlah String dan Kata</h3>
                    </div>
                    <div class="card-body">
                        <?php
                        $soal = "Welcome to php";
                        echo "Soal: ".$soal;
                        echo "<br><br>";
                        $panjang = strlen($soal);
                        $jumlah = str_word_count($soal);
                        echo "panjang string: ".$panjang . " dan jumlah kata : " . $jumlah;
                        ?>
                    </div>
                </div>
                <div class="card shadow mt-4">
                    <div class="card-header">
                        <h3 class="card-title">Tugas No 2 Lower and Upper</h3>
                    </div>
                    <div class="card-body">
                        <?php
                        $soal2 = "SAYA SIAP BELAJAR PHP";
                        echo "Soal: ".$soal2;
                        echo "<br><br>";
                        $lower = strtolower($soal2);
                        echo "hasil dari perubahan kata besar ke kecil : " . $lower;
                        ?>
                    </div>
                </div>
                <div class="card shadow mt-4">
                    <div class="card-header">
                        <h3 class="card-title">Tugas No 3 Mengambil kata pada string dari karakter yang ada di dalamnya</h3>
                    </div>
                    <div class="card-body">
                        <?php
                        $soal3 = "I love Coding";
                        echo "String: ".$soal3;
                        echo "<br><br>";
                        // codingan dimulai
                        $katapertama = substr($soal3, 0, 1);
                        $katakedua = substr($soal3, 2, 5);
                        $kataketiga = substr($soal3, 7, 12);
                        echo "hasil dari Kata Pertama: " . $katapertama;
                        echo "<br><br>";
                        echo "hasil dari Kata Kedua: " . $katakedua;
                        echo "<br><br>";
                        echo "hasil dari Kata Ketiga: " . $kataketiga;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card shadow">
                    <div class="card-header">
                        <h3 class="card-title">Tugas No 1 Menghitung jumlah String dan Kata</h3>
                    </div>
                    <div class="card-body">
                        <?php
                        $soal1 = "I'm ready for the Coding";
                        echo "Soal: ".$soal1;
                        echo "<br><br>";
                        $panjang = strlen($soal1);
                        $jumlah = str_word_count($soal1);
                        echo "panjang string: ".$panjang . " dan jumlah kata : " . $jumlah;
                        ?>
                    </div>
                </div>
                <div class="card shadow mt-4">
                    <div class="card-header">
                        <h3 class="card-title">Tugas No 2 Lower and Upper</h3>
                    </div>
                    <div class="card-body">
                        <?php
                        $soal2_2 = "saya ingin menjadi orang sukses";
                        echo "Soal: ".$soal2_2;
                        echo "<br><br>";
                        $upper = strtoupper($soal2_2);
                        echo "hasil dari perubahan kata kecil ke besar : " . $upper;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>